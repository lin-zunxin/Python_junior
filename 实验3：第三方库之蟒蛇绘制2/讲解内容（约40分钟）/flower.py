import turtle as t #成功滴小红花
t.setup(1000,500)
t.pensize(5)

#花瓣
t.color("black","red")
t.begin_fill()
t.seth(240) #1
t.circle(45,250)

t.seth(360) #2
t.circle(45,250)

t.seth(40) #3
t.circle(45,250)

t.seth(121) #4
t.circle(45,250)

t.seth(210) #5
t.circle(45,250)

t.end_fill()



#花骨朵
t.right(90)
t.color("black","yellow")
t.begin_fill()
t.circle(48,360)
t.end_fill()

t.seth(-90)
t.fd(145)

t.done()